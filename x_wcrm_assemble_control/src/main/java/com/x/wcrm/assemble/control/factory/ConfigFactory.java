package com.x.wcrm.assemble.control.factory;

import com.x.base.core.project.exception.ExceptionWhen;
import com.x.wcrm.assemble.control.AbstractFactory;
import com.x.wcrm.assemble.control.Business;
import com.x.wcrm.core.entity.WCrmConfig;
import com.x.wcrm.core.entity.WCrmConfig_;
import com.x.wcrm.core.entity.tools.CriteriaBuilderTools;
import com.x.wcrm.core.entity.tools.filter.QueryFilter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.List;


public class ConfigFactory extends AbstractFactory {

	public ConfigFactory(Business business ) throws Exception {
		super(business);
	}

	/**
	 * 获取指定Id的Project实体信息对象
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public WCrmConfig get(String id ) throws Exception {
		return this.entityManagerContainer().find( id, WCrmConfig.class, ExceptionWhen.none );
	}

	/**
	 * 根据条件查询符合条件的项目信息ID，根据上一条的sequnce查询指定数量的信息
	 * @param maxCount
	 * @param sequenceFieldValue
	 * @param orderField
	 * @param orderType
	 * @param personName
	 * @param queryFilter
	 * @return
	 * @throws Exception
	 */
	public List<WCrmConfig> listWithFilter( Integer maxCount, Object sequenceFieldValue, String orderField, String orderType, String personName, QueryFilter queryFilter) throws Exception {
		EntityManager em = this.entityManagerContainer().get( WCrmConfig.class );
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WCrmConfig> cq = cb.createQuery(WCrmConfig.class);
		Root<WCrmConfig> root = cq.from(WCrmConfig.class);
		Predicate p_permission = null;

		Predicate p = CriteriaBuilderTools.composePredicateWithQueryFilter( WCrmConfig_.class, cb, p_permission, root, queryFilter );

		if( sequenceFieldValue != null && StringUtils.isNotEmpty( sequenceFieldValue.toString() )) {
			Predicate p_seq = cb.isNotNull( root.get( WCrmConfig_.sequence ) );
			if( "desc".equalsIgnoreCase( orderType )){
				p_seq = cb.and( p_seq, cb.lessThan( root.get( WCrmConfig_.sequence ), sequenceFieldValue.toString() ));
			}else{
				p_seq = cb.and( p_seq, cb.greaterThan( root.get( WCrmConfig_.sequence ), sequenceFieldValue.toString() ));
			}
			p = cb.and( p, p_seq);
		}

		Order orderWithField = CriteriaBuilderTools.getOrder( cb, root, WCrmConfig_.class, orderField, orderType );
		if( orderWithField != null ){
			cq.orderBy( orderWithField );
		}
		return em.createQuery(cq.where(p)).setMaxResults( maxCount).getResultList();
	}

	/**
	 * 根据条件查询所有符合条件的项目模板信息ID，项目信息不会很多 ，所以直接查询出来
	 * @param maxCount
	 * @param personName
	 * @param queryFilter
	 * @return
	 * @throws Exception
	 */
	public List<String> listAllConfigIds( Integer maxCount, String personName, QueryFilter queryFilter) throws Exception {
		EntityManager em = this.entityManagerContainer().get( WCrmConfig.class );
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<String> cq = cb.createQuery(String.class);
		Root<WCrmConfig> root = cq.from(WCrmConfig.class);
		Predicate p_permission = null;

		Predicate p = CriteriaBuilderTools.composePredicateWithQueryFilter( WCrmConfig_.class, cb, p_permission, root, queryFilter );
		cq.distinct(true).select( root.get(WCrmConfig_.id) );
		return em.createQuery(cq.where(p)).setMaxResults( maxCount).getResultList();
	}


	/**
	 * 根据配置类型列示配置信息
	 * @param configModule
	 * @return
	 * @throws Exception
	 */
	public List<WCrmConfig> ListConfigsWithType( String configModule ) throws Exception {
		EntityManager em = this.entityManagerContainer().get(WCrmConfig.class);
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<WCrmConfig> cq = cb.createQuery(WCrmConfig.class);
		Root<WCrmConfig> root = cq.from(WCrmConfig.class);
		Predicate p = cb.equal(root.get(WCrmConfig_.configModule), configModule);
		return em.createQuery(cq.where(p)).getResultList();
	}

}
